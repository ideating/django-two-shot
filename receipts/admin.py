from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from interfaces import AdminModelDisplayList


models = apps.get_app_config("receipts").get_models()

for model in models:
    admin_class = type(
        f"{model._meta.object_name}Admin",
        (AdminModelDisplayList, admin.ModelAdmin),
        {},
    )
    try:
        admin.site.register(model, admin_class)
    except AlreadyRegistered:
        pass
