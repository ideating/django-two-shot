# Generated by Django 4.1.3 on 2022-12-04 01:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0003_alter_account_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expensecategory",
            name="name",
            field=models.CharField(max_length=51),
        ),
    ]
