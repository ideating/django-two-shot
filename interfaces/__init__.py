class AdminModelDisplayList(object):
    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.get_fields()]
        super(AdminModelDisplayList, self).__init__(model, admin_site)
